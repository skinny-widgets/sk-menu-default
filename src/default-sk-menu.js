
import { SkMenuImpl } from "../../sk-menu/src/impl/sk-menu-impl.js";

export class DefaultSkMenu extends SkMenuImpl {

    get prefix() {
        return 'default';
    }

}
